import traceback
import mlm
import os.path 
import saveall as sv
import find 

def save(filename_jbl, *list_var): #, force = False):
#def save(filename_jbl, *list_var, force = False):
  

    saving_data = True 

    if  os.path.exists(filename_jbl) == True: # and force == False:
        print(os.path.exists(filename_jbl))
        overwrite_yn = input('File {} exists. Do you want to overwrite it?  Y/[n] '.format(filename_jbl))
        if overwrite_yn != 'Y': saving_data = False

    if saving_data ==  True:
        (filename,line_number,function_name,text)=traceback.extract_stack()[-2]
        begin=text.find('save(')+len('save(')
        end=text.find(')',begin)
        text=[name.strip() for name in text[begin:end].split(',')]
        dict_var = dict(zip(text[1:],list_var))

        for keys in dict_var.keys(): print(keys)

        mlm.save(filename_jbl, dict_var,  check_file_exists = False)
    else:
        print('Nothing has been done.')
 
    return

def load(filename_jbl, ver=False, list_var = False):
    
    dict_var = mlm.load(filename_jbl)

    (filename,line_number,function_name,text_ori)=traceback.extract_stack()[-2]
    end=text_ori.find('=')
    text=text_ori[0:end].replace(' ','')
    text=[name.strip() for name in text[0:end].split(',')]

    print(len(text))
    count = 0
    txt = '' 
    print()
    print('Suggested command:')
    print()
    if len(text) > 1 and list_var == False: print('{} = {}'.format(text[0], text_ori[end:]))
    for i in dict_var.keys(): print("{} = {}['{}']".format(i, text[0], i))
    print()
    print('or')
    print()
    for i in dict_var.keys(): txt = "{}, ".format(i)+txt
    txt = txt[:-2]
    print(txt+' ', text_ori[end:].replace('False', 'True'))
    print()
    if ver == True:
        for i in dict_var.keys(): print("{} : {}".format(i, type(dict_var[i])))
        print()
  
    out = dict_var
    if list_var == True: 
        out = []
        for i in dict_var.keys(): out.append(dict_var[i])

    return out  


def list(ext='.jbl.gz'):

    files = find.find('./', '*{}'.format(ext))

    for j,i in enumerate(files):
        print(j, i)
   
    aux =  None 
    sel = input('Select a file {} to be loaded: '.format(ext))
    sel = int(sel)
    if sel > -1 and sel < j: 
        aux = sv.load(files[j])

    return aux     

def arguments():
    """Returns tuple containing dictionary of calling function's
       named arguments and a list of calling function's unnamed
       positional arguments.
    """
    from inspect import getargvalues, stack
    posname, kwname, args = getargvalues(stack()[1][0])[-3:]
    posargs = args.pop(posname, [])
    args.update(args.pop(kwname, []))

    return args, posargs

def make_dict(*expr):
    (filename,line_number,function_name,text)=traceback.extract_stack()[-2]
    begin=text.find('make_dict(')+len('make_dict(')
    end=text.find(')',begin)
    text=[name.strip() for name in text[begin:end].split(',')]

    return dict(zip(text,expr))
